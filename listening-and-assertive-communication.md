# Listening and Active Communication

## 1. What are the steps/strategies to do Active listening?
* Avoid getting distracted by your thoughts.
* Try not to interrupt the other person.
* Show that you are listening with your body language.
* Take notes during an important conversation.
* Focus on the speaker and topics.
* Use door openers that show that you are interesting and keep the other person talking.

## 2. What are the key points of Reflective Listening?
* Listen to the speaker's message and analyze the meaning of the message.
* Reflect on the message to the speaker and confirm that what you understood is correct.
* Listen more than you talk.
* Restate and clarify what the other has said. Don't ask questions or say what you believe, feel or want.
* Responding with acceptance or empathy, not with fake concern.
* Try to understand the feelings of the message.

## 3. What are the obstacles in your listening process?
* Noisy environment.
* Distracted by something else in the environment.
* The content is loaded with full of information.
* Mental Laziness.
* The rate of speech is too fast.
* Sometimes language barrier, not able to understand the message properly.
## 4. What can you do to improve your listening?
* Practice Listening.
* Listen without distraction.
* Occasionally ask questions to the speaker for clarifying doubts.
* Try to catch the non-verbal clues.
* Don't interrupt.
## 5. When do you switch to a passive communication style in your day-to-day life?
* If I do have not enough knowledge in a particular topic, and the discussion is all about that.
* If the person is closely related to me and I don't want that person to get hurt.
* If the person is superior to me.
## 6. When do you switch to an aggressive communication style in your day-to-day life?
* Mostly I don't prefer aggressive communication. If in case things are not bearable then I can think to switch to aggressive communication.
* If one person has no idea about a topic but he/she tries to prove wrong others by arguing.
* When someone shows bad behaviour.
## 7. When do you switch to a passive-aggressive communication style in your day-to-day life?
* When someone ignores me without any reason but after a few days he/she shows some fake showoff for making their work.
* When someone gives the silent treatment.
* When I got to know someone talking bad about me behind my back.
## 8. How can you make your communication assertive?
* I can practice these skills in situations that have low risk like with a friend.
* Practice saying no without hesitation.
* Thinks about own satisfaction rather than keeps deprecating own satisfaction.
* By using body language.
* Keep your thoughts without disrespecting others.