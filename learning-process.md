# Learning Process
## How to Learn Faster with the Feynman Technique
### 1. What is the Feynman Technique?
If you want to understand something well, explain it.
This technique is named after the physicist Richard Feynman. He was also a great scientist and teacher. in addition to this, he is also known as "The great explainer", because he used to explain complex topics in simple language so that other people can understand. In his time of learning, he was also working with the concept till it is easy to understand.

The first principle of Feynman - 
> The first prinicple is that you must not fool yourself, and you are the easiest person to fool.

There are 4 steps for using this technique.

* Write down the name of the topics at the top of the paper.
* Explain the concept using simple language.
* Identify the area where your explanation is shaky. Then go back to the resources for review.
* Find out the area where the explanation contains more technical terms and complex language, and try to make it simpler.

### 2. What are the different ways to implement this technique in your learning process?
* We have to write down the name of the topics in a paper.
* After completing one topic, we can try to explain that topic to friends.
* We have to find in which area my explanation is not good enough or shaky, then we have to again review the topic.
* During the explanation, if some point contains complex language, we can rethink that and make it simple as possible.
* We can search for some questions regarding our learning, and check whether we know all the answers. By this, we can ensure that how much we good in that topic.

## Learning How to Learn TED talk by Barbara Oakley

### 3. Paraphrase the video in detail in your own words.
* As we already the functionality of the brain is very complex. But we can simplify it by using two methods i.e focused and diffuse.
* In focus mode, we simply focus on a particular thing and our brain stops thinking about any other things.
* In diffuse mode, the brain is in a relaxed state. When we got stuck in some problem. We should switch to diffuse mode so that we can think of some other idea that might helpful for solving the problem.
* During learning, some people learn fast while some are slow. Usually, people who are slow learners, need to work hard but at the same time, they have a higher chance of deep understanding of the topic as they spent more time on that.
* We should take a break after some learning and utilize that time which makes us happy.

### 4. What are some of the steps that you can take to improve your learning process?
* Make the points which things I have to learn in the particular period.
* During learning, take study breaks regularly for some time if needed.
* There is some chance of forgetting past learning. we should recall it after some intervals.
* Make notes it will be helpful to find any point during recall. which may save a lot of time during learning.
* Discuss with friends, may give some new information which we missed during learning.
* Try to learn in a new way like through some graphical presentation or game. It will make learning more interesting.

# Learn Anything in 20 hours

### 5. Your key takeaways from the video? Paraphrase your understanding.
* When we start to learn things it gets more time, but after some practice, it gets easier and takes less time.
* Some research says we need 10,000 hours to learn a skill which is a very long period.
* But in this video, we got to know if we practice 20 hours with focus. We will be very good at that skill.
Daily 45 minutes for 1 month will be enough. 

**Deconstruct the skill**

Decide what exactly we want to do, what to learn, and how this skill impacts our life. Break down the skills into smaller parts. We have to practice the most important things first.

**Learning enough to self-correct**

Find out the resource from which we have to learn it may be books, courses etc. Eliminate the things which you do wrong, and be on the correct path. 

**Remove Practice barriers**

Remove all the barriers which may create a distraction during practice it may be television, mobile, social media etc.

**Practice at least 20 hours**

Practice the skills for at least 20 hours with full dedication and without negligence.

### 6. What are some of the steps that you can while approaching a new topic?

* Firstly we have to decide what topic we want to learn, and how it can be helpful for us.
* Break the topic into smaller parts, organize the path correctly, and make sure that we start from the basic.
* Find out the courses, and books which we have to follow.
* Set a time limit, and try to complete that topic in that fixed time.
* Practice enough while learning the topic.

## References used for this project
* [Feynman Technique](https://www.youtube.com/watch?v=_f-qkGJBPts)
* [Learning How to Learn TED talk by Barbara Oakley](https://www.youtube.com/watch?v=O96fE1E-rf8)
* [Learn Anything in 20 hours](https://www.youtube.com/watch?v=5MgBikgcWnY)