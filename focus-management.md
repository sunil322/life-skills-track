# Focus Management
## 1. What is deep work?
* Deep work refers to focusing without distraction on a cognitively demanding task.
* It is useful when we want to produce high-quality work in a shorter amount of time by eliminating distractions.

## 2. Paraphrase all the ideas in the above videos and this one in detail.
* The optimal duration for deep work varies person to person, but usually, it should be at least 1 hour. Because focusing for a long time is not possible for all.
* Try not to switch between contexts during the deep work period.
* Breaks and distractions must be scheduled.
* Deadlines/time-block schedules are a kind of motivational signal, it helps one by avoiding procrastinating things and avoiding taking unnecessary breaks in middle.
* Deadlines should be reasonable to a humane capacity, sometimes too much pressure can reduce the productivity.
* Deep work must be made a habit for better productivity.

## 3. How can you implement the principles in your day-to-day life?
* Practice deep work starting from a small amount of time, and keep increasing time day by day.
* Eliminate distractions during deep work.
* Don't use a smartphone or social media during deep work.

## Dangers of Social Media
## 4 . Your key takeaways from the video.
* Social media can be addictive which leads to excessive use of it, eventually, it wastes a lot of time.
* Social media can make a negative impact on mental health by promoting unrealistic expectations which lead to depression, anxiety etc.
* Also, sometimes social media play a huge role in spreading false information.
* Some people make the wrong usage of social media, by collecting some information about others and using that information to harass others.
* Social media collects so much information about users, and use that to show advertisements or sold them to other companies.
* When we spend so much time on social media, which may decrease face-to-face communication skills. 

## References
* [What is deep work?](https://www.youtube.com/watch?v=b6xQpoVgN68)
* [Optimal duration for deep work](https://www.youtube.com/watch?v=LA6mvxwecZ0)
* [Are deadlines good for productivity?](https://www.youtube.com/watch?v=Jkl1vMNvvHU)
* [Summary of Deep Work Book](https://www.youtube.com/watch?v=gTaJhjQHcf8)
* [Dangers of Social Media](https://www.youtube.com/watch?v=3E7hkPZ-HTk)