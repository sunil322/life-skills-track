# Prevention of Sexual Harassment
## What kinds of behaviour cause sexual harassment?
* Comments about a person's body.
* Comments about someone's clothing.
* Making sexual or gender-based remarks or jokes.
* Requesting sexual favours or repeatedly asking a person out.
* Spreading rumours about a person's personal or sexual life.
* Sending emails or texts of sexual nature.
* Use Foul and obscene language.
* Inappropriate touching.
* Threatening or offering rewards to a person for getting sexual favour (Quid Pro Quo).
* Make sexual gesturing.

## What would you do in case you face or witness any incident or repeated incidents of such behaviour?
* Let the person know that their behaviour is inappropriate, and warn the person not to do such things again in future.
* If the person continuously repeated this type of incident, then I will inform the higher authorities.
* Provide emotional support to the person who is being harassed.