# OSI Layer
OSI stands for open systems Interconnection. It has been developed by ISO(International Organization for Standardization) in 1984. This model consists of 7 layers, it defines how the data has been transmitted from one computer to another through the computer network.

![OSI-model](https://media.geeksforgeeks.org/wp-content/uploads/computer-network-osi-model-layers.png)


## 1. Application Layer
This layer is used by network applications such as web browsers, skype etc. This layer is responsible for displaying the received information to the user. It also produces the data which has to be transmitted over the network.

### The functions of the application layer are
* Network Virtual Terminal
* Mail Services
* File Transfer
* Directory Services
## 2. Presentation layer
This layer received the data from the application layer. It converts the data to binary format. It also compresses the data by reducing the number of bits. The data encryption and decryption are also done in this layer by using a key value. 

### The functions of the presentation layer are
* Translation 
* Encryption/Decryption
* Compression
## 3. Session Layer
This layer is responsible for the establishment, maintenance and termination of the session. Before the establishment of a connection the server performs authentication after successful authentication, a connection got established between the two systems. When a user wants to get some file it also performs authorization for verifying if the user has rights to access this resource.

### The functions of the session layer are
* Session establishment, maintenance and termination
* Synchronization
* Dialog Controller
## 4. Transport Layer
The data received from the session layer is divided into smaller units, this unit referred to as segments. Each segment contains information on the source and destination port number. It also contains the sequence number by which it can arrange in the right order during serving the message. It is also responsible for the end-to-end delivery of the complete message. If any error occurs then it will re-transmit the message. It performs two types of services i.e. connection-oriented service, and connection-less service. 
### The functions of the transport layer are
* Segmentation and Reassembly
* service Point Addressing
## 5. Network Layer
This layer is responsible for transmitting the data from one computer to other. In this layer, the IP address is placed for the sender and receiver. The data units in this layer are called packets. This layer is also responsible for packet routing which determines the shortest path for transmitting the packet from source to destination.

### The functions of the network layer are
* Packet Routing
* Logical Addressing
* Path Determination
## 6. Data Link layer
This layer received the data packets from the network layer. This packet contains the IP address of the sender and receiver. In this layer, the MAC address of the sender and receiver is assigned to the packet. It is also responsible for making sure that the data transfer is error-free.

### The functions of the data link layer are
* Framing
* Physical Addressing
* Error Control
* Flow Control
* Access Control

## 7. Physical layer
This layer is responsible for the physical connection between devices. The data received by the physical layer is in binary format, then it converts this data into signals and transmits over local media. This layer also defines the transmission rate and specifies how the devices are arranged in a network such as star, bus, ring etc.

### The functions of the physical layer are
* Bit Synchronization
* Bit rate control
* Physical topologies
* Transmission mode

#### References used for the report 
* [Youtube video (OSI Model Explanation)](https://youtu.be/vv4y_uOneC0)
* [Layers of OSI model](https://www.geeksforgeeks.org/layers-of-osi-model/) 