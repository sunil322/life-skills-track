# Good Practices for Software Development
## Q1. What is your one major takeaway from each one of the 6 sections?
### 1. Gathering requirements
* Make sure to ask questions and seek clarity in the meeting itself. Since most of the time, it will be difficult to get the same set of people online again.
### 2. Always over-communicate: Some scenarios
* Implementation is taking longer than usual due to some unexpected issue - Inform relevant team members.
### 3. Stuck? Ask questions
* Explain the problem clearly, and mention the solutions you tried out to fix the problem.
### 4. Get to know your teammates
* Make time for your company, the product you are working on, and your team members. This will help a lot in improving your communication with the team.
### 5. Be aware and mindful of other team members
* Remember they have their work to do as well. Pick and choose your communication medium depending on the situation.
### 6. Doing things with 100% involvement
* Make sure you do some exercise to keep your energy levels high throughout the day.

## Q2. Which area do you think you need to improve on? What are your ideas to make progress in that area?
* I will make sure to be clear about the requirements, and take notes during the discussion.
* If I got stuck, then I will ask the question by explaining the problem clearly.
* I will use time-tracking apps like `Boosted` to improve my productivity.