### Q1. What are the activities you do that make you relax - Calm quadrant?
* Listening music.
* Playing games.
* Watching comedy videos or movies.

### Q2. When do you find getting into the Stress quadrant?
* When I have less time and in that time I have to do a lot of work.
* When I was stuck on a problem after applying many ways but still not able to figure out the solution.

### Q3. How do you understand if you are in the Excitement quadrant?
* When I am stuck on something for a long time, and finally able to figure it out.
* When I help any needy person.

### Q4. Paraphrase the Sleep is your Superpower video in detail.

* A person should take 8 hours of sleep per day.
* Lack of sleep will age a man by a decade.
* The person who sleeps 7-8 hours, whose brain is 40% more efficient than the person who sleeps only 4-5 hours.
* Also, the research found that less sleep affects our immune system.
* Lack of sleep also raises the chances of cancer.
  
### Q5.What are some ideas that you can implement to sleep better?
* Make a proper routine of 8 hours of sleep per day.
* Maintain a good temperature in the room, so that it will help to sleep easily.
* Make sure that no loud outside noises come into the room, if noises there close all the windows and doors to reduce noise.

### Q6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.
* The main two key areas of our brain i.e. prefrontal cortex and hippocampus.
* The Prefrontal cortex is for things like decision making, focus, attention and personality. 
* Hippocampus is for the ability to form and retain long-term memories of facts and events.
* Exercise produces new brain cells, which increases the volume of our hippocampus. Eventually, it increases long-term memory.
* Regular exercise helps to improve our memory and thinking ability.
* We can focus on some work for a long time.
### Q7. What are some steps you can take to exercise more?
* By adding exercise to the daily routine.
* Use of stairs if the destination is between 2-3 floors.
* Go by walk if the destination is not so far.