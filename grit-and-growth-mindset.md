# Grit and Growth Mindset

## 1. Grit

### 1. Paraphrase (summarize) the video in a few lines. Use your own words.
* In education, we know IQ is the key point for measurement, but it didn't mean that person having a better IQ will perform better if a person works hard whether having a low IQ or a better IQ, that person will perform well.
* Grit is all about having passion and perseverance for the things we want to achieve. We have to work hard for learning things.
* Anyone can learn anything if the person has eager to learn and put in the effort that requires.
* Grit helps life long us for being better.

### 2. What are your key takeaways from the video to take action on?
* To put full effort into learning things.
* Be motivated to do things.
* Work hard if it requires to get better.
## 2. Introduction to Growth Mindset

### 3. Paraphrase (summarize) the video in a few lines in your own words.
* People have fixed mindsets, they believe that you are not control in of your abilities. They believe all have fixed skill sets.
* People have growth mindsets, they believe that you are in control of your abilities. It means a person can build any skill. If the person works on that skill.
* The key characteristics of a growth mindset.
   - Putting effort
   - Don't avoid challenges
   - Accepting mistakes and learning from the mistake.
   - Accepting feedbacks.
### 4. What are your key takeaways from the video to take action on?
* Keep self-belief and put effort into doing things.
* Focus on the process of how to get better by learning.
* Accepting feedback and learning from mistakes.
## 3. Understanding Internal Locus of Control
### 5. What is the Internal Locus of Control? What is the key point in the video?
* The degree to which you believe you have control over your life.
* Solving the problems in your life, and keep appreciating yourself that you took that step.
## 4. How to build a Growth Mindset
### 6. Paraphrase (summarize) the video in a few lines in your own words.
* Believe in your ability to figure out things.
* Question your assumptions.
### 7. What are your key takeaways from the video to take action on?
* Whatever difficulty comes you should believe in yourself that you can figure it out.
* If you are struggling with something, don't quit. This difficulty helps you to strengthen yourself, which can help you in the future.
### 8. What are one or more points that you want to take action on from the manual?
* I will stay with a problem till I complete it. I will not quit the problem.
* I will understand each concept properly.
* I know more efforts lead to better understanding.
 
### References used for this report
* [Grit Video](https://www.youtube.com/watch?v=H14bBuluwB8)
* [Introduction to Growth Mindset Video](https://www.youtube.com/watch?v=75GFzikmRY0)
* [How to stay motivated Video]( https://www.youtube.com/watch?v=9DVdclX6NzY)
* [Link to Google Docs - Mindset](https://docs.google.com/document/d/1SPUqC-8WwfiDlsRGKWqoMtC14v6_2TEhq7LZs29bJWk/edit)